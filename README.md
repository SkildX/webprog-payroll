# README/Information Board

This is the repository for the requirements for Finals on WEBPROG 3rd year - 1st Semester. The task is to make a simple payroll system according to this case problem found here:  

### Case Problem
```
https://goo.gl/rnVD2G
```  
This repository is set to close and never be updated once submitted and approved by the professor but shall be left untouched after for future references and use of the contributors.
  
### Developer's Requirements  
* Git
* PHP 5 above installed
* Your own IDE (Notepad++  & Sublime Text are good enough)
* XAMPP as server
  
  
# Knowledge Requirements upon using Git
For those who are new, you won't understand mostly what I wrote from **Contribution Guidlines** below so if you don't understand it's better you start off from these. I compiled good tutorials for you to get started. I know it's a lot for you but an average person can learn it for a day actually so don't get to stressed on the number of tutorials I gave and those tutorials are short and sufficient enough to understand the concepts. If you don't understand. Talk to the admin, they can help but they also need your help to understand it quick enough. You have Google to so don't worry.

1. Basic understanding of basic Git commands (*e.g. git add, git commit, git status, git branch, git revert*)  
    **Learn it here**
    ```https://www.youtube.com/watch?v=0fKg7e37bQE```  
    
2. Basic understanding about the concept of Git branches.
    **Understand it here in 8 minutes**
    ```https://www.youtube.com/watch?v=LbanWJyLMnA```

3. Basic understanding on how collaboration works online.
    **Here's a good video**
    ```https://www.youtube.com/watch?v=BFO8G6piY0c```  
  
  
# Contribution guidelines
Collaboration rules are simple. Here are the most simple steps your could follow if you are a new.  
  
## Steps (In Order)
1. Clone the repository if you still haven't got the copy of the repository to your local computer
```git clone <link of the repository>```
    *Sign in to your Bitbucket account if required*
  
2. Commit your changes to your own branch  
  
    **Note:** Always use your another branch not "master" since it is the safest way and a handy way to make a pull request. This is for the safety of the release. The golden rule of all is to never apply/commit your changes to "master" since it is a very bad practice and you may break/lose your progress.

3. Once ready upload your branch with ```git push -u origin <your branch name>``` and open a Pull Request from your BitBucket Dashboard.  
  
    **Here's a demo for Pull Request:**  
    ```https://www.youtube.com/watch?v=ssDHUyrQ8nI```  

4. People who are involve in this repository will review/discuss your code to find issues or suggest some features or enhancement to your contribution.

5. If people and the admin is satisfied to your feature, it will be approved by the admins and merge it to the branch they like.
  
  
## Important Things to Consider
1. **Always Pull using the "master" branch** even though there are no updates. Pull is the way you grab the update directly from the remote repository to your local workspace. You need to sync your local copy all the time to avoid conflicts that will drive you nuts sometimes.  
  
    **Pull Command**  
    ```git pull```  
  
    **Note:** Pull will automatically work if you clone this project since the repository knows where to get the files already.

2. Always upload your branch and open the Pull Request yourself. That way, you can apply more follow-up details about what's new on your commits.

# ADMINS
* Jasper Lance C. Dayon
* Johndel B. Villanueva