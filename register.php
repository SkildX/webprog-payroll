<?php include "php/validate.php";?>

<html>
    
    <header>
        <style>
            
            *{
                font-style: sans-serif;    
            }
            
            body{
                position: absolute;
                top: 50%;
                left: 50%;
                
                transform: translate(-50%, -50%);
            }
            
            .inputs, #back{
                height: 40px; width: 300px;
                margin: 5px 0px 0px 0px;
            }
            
            #submit{
                height: 70px;
                border-style: none;
                background-color: blueviolet;
                color: white;
                font-size: 13px;
                cursor: pointer;
            }
            
            #back{
                height: 70px;
                border-style: none;
                background-color: #f90000;
                color: white;
                font-size: 13px;
                margin: 0;
                cursor: pointer;
            }

        </style>

        <title>Banana - Register</title>
        <center><h2 style="font-family: arial; margin-bottom: 35px;">Register Banana Account</h2></center>
    </header>
    
    <body>
        <div>
            <form action="php/register.php" method="POST">
                <table>
                    <tr>
                        <td><input class="inputs" type="text" name="username" placeholder="Username"></td>
                    </tr>

                    <tr>
                        <td><input class="inputs" type="password" name="password" placeholder="Password"></td>
                    </tr>

                    <tr>
                        <td><input class="inputs" type="text" name="first_name" placeholder="First Name"></td>
                    </tr>

                    <tr>
                        <td><input class="inputs" type="text" name="middle_name" placeholder="Middle Name"></td>
                    </tr>

                    <tr>
                        <td><input class="inputs" type="text" name="last_name" placeholder="Last Name"></td>
                    </tr>

                    <tr>
                        <td><p>Access Level</p></td>
                    </tr>

                    <tr>
                        <td>
                            <select class="inputs" name="access_level">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td><input class="inputs" id="submit" type="submit" name="register" value="REGISTER"></td>
                    </tr>
                    
                    <tr>
                        <td><a href="main.php"><button id="back" type="button">BACK TO MAIN</button></a></td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>