<?php
include "../../php/validate.php";
include "../../php/dbconnect.php";

$id = $_GET['update'];

$dbq_cutoff = mysqli_query($conn,"SELECT * FROM cutoff WHERE cutoffID = '$id'");
$dbq_cutoff_row = mysqli_fetch_assoc($dbq_cutoff);
?>

<html>
<title>Create Cutoff</title>

<style>
    *{
        font-family: Arial;
    }

    input, button{
        height: 30px;
        font-size: inherit;
    }

    input[type=submit], button{
        width: 175px;
    }

    body{
        position: absolute;
        top: 50%;
        left: 50%;

        transform: translate(-50%,-50%);
    }

    form{
        margin: 0px;
    }

    label{
        margin: 2px;
    }
</style>

<body>
<div>
    <center><h3>Create Cutoff</h3></center>
    <form action="" method="post">
        <table>
            <tr>
                <td>
                    <label>From</label><br>
                    <?php
                    echo "<input type=\"date\" name=\"dateFrom\" value='".$dbq_cutoff_row['start_date']."'>"
                    ?>
                </td>
            </tr>

            <tr>
                <td>
                    <label>To</label><br>
                    <?php
                    echo "<input type=\"date\" name=\"dateTo\" value='".$dbq_cutoff_row['end_date']."'>";
                    ?>
                </td>

            </tr>
        </table>

        <input class="register" type="submit" name="btnupdate" value="UPDATE">
    </form>
    <a href="../pickpayroll.php"><button>CANCEL</button></a>
</div>
</body>
</html>

<?php

if(isset($_POST['btnupdate'])){
    $from = $_POST['dateFrom'];
    $to = $_POST['dateTo'];

    //update record based on cutoff ID and redirect back to pickpayroll.php
    mysqli_query($conn,"UPDATE cutoff SET start_date = '$from',end_date = '$to' WHERE cutoffID = '$id'");
    header("location: ../pickpayroll.php");
}
?>
