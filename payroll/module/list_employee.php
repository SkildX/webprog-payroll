<html>
<title>Employee Selection</title>

<style>
    *{
        font-family: Arial;
    }

    td,th{
        border-style: solid;
        border-width: thin;
        width: 200px;
    }

    button{
        width: 100%;
    }
</style>

<body>
<center>
<?php
include "../../php/validate.php";
include "../../php/dbconnect.php";

//store cutoff id to session
if(!(isset($_SESSION['id_cutoff'])))
    $_SESSION['id_cutoff'] = $_GET['enc_cutoff'];

$qdb_employees = mysqli_query($conn,"SELECT * FROM employees ORDER BY lname ASC ");


echo "<table>";
echo "<tr>";
echo "<th>Name</th> <th>Department</th> <th>Menu</th>";
echo "</tr>";
while ($qdb_employees_row = mysqli_fetch_assoc($qdb_employees)){

    echo "<tr>";
    echo "<td>".$qdb_employees_row['lname'].", ".$qdb_employees_row['fname']." ".$qdb_employees_row['mname']."";
    echo "<td>".$qdb_employees_row['department']."</td>";
    echo "<td><a href='encode_cutoff.php?emp=".$qdb_employees_row['emp_id']."'><button>Select</button></a></td>";
    echo "</tr>";
}
echo "</table>";
?>
</center>
</body>

</html>
