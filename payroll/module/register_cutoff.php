<?php
include "../../php/validate.php";
include "../../php/dbconnect.php";
?>

<html>
<title>Create Cutoff</title>

<style>
    *{
        font-family: Arial;
    }

    input, button{
        height: 30px;
        font-size: inherit;
    }

    input[type=submit], button{
        width: 175px;
    }

    body{
        position: absolute;
        top: 50%;
        left: 50%;

        transform: translate(-50%,-50%);
    }

    form{
        margin: 0px;
    }

    label{
        margin: 2px;
    }
</style>

<body>
<div>
    <center><h3>Create Cutoff</h3></center>
    <form action="" method="post">
        <table>
            <tr>
                <td>
                    <label>From</label><br>
                    <input type="date" name="dateFrom">
                </td>
            </tr>

            <tr>
                <td>
                    <label>To</label><br>
                    <input type="date" name="dateTo">
                </td>

            </tr>
        </table>

        <input class="register" type="submit" name="register" value="CREATE">
    </form>
    <a href="../pickpayroll.php"><button>CANCEL</button></a>
</div>
</body>
</html>

<?php

if(isset($_POST['register'])){
    $from = $_POST['dateFrom'];
    $to = $_POST['dateTo'];

    //insert values
    mysqli_query($conn,"INSERT INTO `cutoff`(`start_date`, `end_date`) VALUES ('$from','$to')");

    header("location: ../pickpayroll.php");
}

?>
