<html>
<title>Encode Cutoff</title>

<style>
    *{
        font-family: Arial;
        font-size: 13px;
    }
</style>

<body>
<div>
    <form method="post">
        <h3>Encode Cutoff</h3>
        <?php
        include "../../php/validate.php";
        include "../../php/dbconnect.php";

        $employee = $_GET['emp'];
        $cutoff = $_SESSION['id_cutoff'];

        //query employee and store information to a 'readlony' textbox
        $qdb_employee = mysqli_query($conn,"SELECT * FROM employees WHERE emp_id = '$employee'");
        $qdb_employee_row = mysqli_fetch_assoc($qdb_employee);

        //output information
        echo "<table>";

        //name
        echo "<tr>";
        echo "<td><b>Name:</b> ".$qdb_employee_row['lname'].", ".$qdb_employee_row['fname']." ".$qdb_employee_row['mname']."</td>";
        echo "</tr>";

        //department
        echo "<tr>";
        echo "<td><b>Department:</b> ".$qdb_employee_row['department']."</td>";
        echo "<td>|| <b>Rate:</b> PHP ".$qdb_employee_row['rate']."</td>";
        echo "</tr>";

        //cutoff
        //query cutoff and display
        $qdb_cutoff = mysqli_query($conn,"SELECT * FROM cutoff WHERE cutoffID = '$cutoff'");
        $qdb_cutoff_row = mysqli_fetch_assoc($qdb_cutoff);

        echo "<tr>";
        echo "<td><b>Cutoff:</b> ".$qdb_cutoff_row['start_date']." to ".$qdb_cutoff_row['end_date']."";
        echo "</tr>";

        echo "</table>";

        //days
        echo "<label>Days of Duty</label><br />";
        echo "<input type='number' name='days' placeholder='Days'><br />";

        //cash advance
        echo "<label>Cash Advance</label><br />";
        echo "<input type='number' name='ca' placeholder='Amount'> <br />";

        //encode button
        echo "<input type='submit' name='encode' value='Encode'>";
        ?>
    </form>
    <a href="list_employee.php"><button>Back</button></a>
</div>
</body>
</html>


<?php

if(isset($_POST['encode'])){
    //preset variables
    $emp_id = $qdb_employee_row['emp_id'];
    $cutoff_id = $qdb_cutoff_row['cutoffID'];

    //inputs
    $days = $_POST['days'];
    $ca = $_POST['ca'];

    //echo var_dump($emp_id)."<br />";
    //echo var_dump($cutoff_id)."<br />";

    //check if the current employee exist on current cutoff
    $qdb_duplicate = mysqli_query($conn,"SELECT emp_id FROM payroll WHERE cutoffID = '$cutoff_id' AND emp_id = '$emp_id'");
    $qdb_duplicate_row = mysqli_num_rows($qdb_duplicate);

    //var_dump($qdb_duplicate_row);

    if($qdb_duplicate_row >= 1)
        die("Employee already encoded on current Cutoff");

    //insert payroll of the employee by cutoff
    mysqli_query($conn,"INSERT INTO `payroll`(`emp_id`, `duty_days`, `cash_adv`, `cutoffID`)
                              VALUES ('$emp_id','$days','$ca','$cutoff_id')");

    //redirect to pickpayroll.php per successful execution
    header('location: ../pickpayroll.php');
}

?>