<?php
	ob_start();
    include "../php/validate.php";
	include ("../php/dbconnect.php");
?>

<html>
<style type="text/css">
	a
	{
		color: black;
		text-decoration: none;
	}
</style>
<body>
	<center>
		<td valign="top" align="center">
				<?php
					// If clicked, update database
					if(isset($_GET['submit']))
					{
						$emp_id = $_GET['emp_id'];
						$fname = $_GET['fname'];
						$mname = $_GET['mname'];
						$lname = $_GET['lname'];
						$depmt = $_GET['depmt'];
						$rate = $_GET['rate'];
						$query = mysqli_query($conn, "UPDATE employees SET fname='$fname', mname='$mname', lname='$lname', 
							department='$depmt', rate='$rate' WHERE emp_id='$emp_id'");
					}
				?>

				<?php
					// Retrieve database entry
					if(isset($_GET['update']))
					{
						$update = $_GET['update'];
						$query1 = mysqli_query($conn, "SELECT * FROM employees WHERE emp_id='$update'");
						echo "<table>";
						while ($row1 = mysqli_fetch_array($query1))
						{
							echo "<form class='form' method='get'>";
							echo "<td><input class='input' type='hidden' name='emp_id' value='{$row1['emp_id']}'></td>";
							echo "<tr colspan='2'><h2>Update Employee</h2></tr>";
							echo "<hr>";
							echo
							"
							<tr>
								<td><label>First Name:</label></td>
								<td><input class='input' type='text' name='fname' value='{$row1['fname']}' required></td>
							</tr>
							";
							echo
							"
							<tr>
								<td><label>Middle Name:</label></td>
								<td><input class='input' type='text' name='mname' value='{$row1['mname']}'></td>
							</tr>
							";
							echo
							"
							<tr>
								<td><label>Last Name:</label></td>
								<td><input class='input' type='text' name='lname' value='{$row1['lname']}' required></td>
							</tr>
							";
							echo
							"
							<tr>
								<td><label>Department:</label></td>
								<td>
									<select name='depmt' required>";

									$options = array(
												"Live Stock Department",
												"Palay Lands (Humayan)",
												"Motorpool",
												"Vegetables Farm",
												"Coconut Area",
												"Sugarcane",
												"Voucher Employees"
												);

									for ($i = 0; $i < count($options); $i++)
									{
										echo "<option value='$options[$i]'>$options[$i]</option>";
									}
									echo "
									</select>
								</td>
							</tr>
							";
							echo
							"
							<tr>
								<td><label>Rate:</label></td>
								<td><input class='input' type='text' name='rate' value='{$row1['rate']}'></td>
							</tr>
							";

							// Please Add Cancel Function
							echo
							"
							<tr align='center'>
								<td><input class='submit' type='submit' name='submit' value='Update'></td>
							</tr>
							";
							echo "</form>";
							echo "<td><a href='viewemployees.php'><button>Cancel</button></a></td>";
						}
						echo "</table>";
					}

					if(isset($_GET['submit']))
					{
						echo "Employee Updated";
						header("refresh: 2; url=viewemployees.php");
					}
				?>
	</center>

	<?php
		mysqli_close($conn);
	?>
</body>
</html>