<?php
    include "../php/validate.php";
	include("../php/dbconnect.php");
?>

<html>
<style type="text/css">
	a
	{
		text-decoration: none;
		color: black;
	}
</style>
<body>
	<center>
		<?php
			$result = mysqli_query($conn, "SELECT * FROM employees ORDER BY emp_id");
			echo
			"
				<label style='font-size: 28'><b>LIST OF EMPLOYEES</b></label><br>
				<table cellspacing='15' width='60%'>
					<tr>
						<th>First Name</th>
						<th>Middle Name</th>
						<th>Last Name</th>
						<th>Department</th>
						<th>Rate</th>
						<th>Options</th>
					</tr>
			";
			echo "<hr>";

			while ($row = mysqli_fetch_array($result))
			{
				echo "<tr align='center'>";
				echo "<td>".$row['fname']."</td>";
				echo "<td>".$row['mname']."</td>";
				echo "<td>".$row['lname']."</td>";
				echo "<td>".$row['department']."</td>";
				echo "<td>".$row['rate']."</td>";
				echo
				"
					<td>
						<button><a href=\"updateemployees.php?update=$row[emp_id]\">Update</a></button> |
						<button><a href=\"deleteemployee.php?id=$row[emp_id]\" onClick=\"return confirm('Are you sure you want to delete?')\">Remove</a></button>
					</td>
				";
				echo "</tr>";
			}
			echo "</table>";
			echo "<hr>";
			echo "<button><a href='addemployees.php'><b>+</b> | Add new employee</a></button>";
            echo "<button><a href='../main.php'>Back</a></button>";
		?>
	</center>
</body>
</html>