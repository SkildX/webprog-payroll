<?php
include "../php/validate.php";
include("../php/dbconnect.php");
?>

<html>

<header>
    <style>
        .btn:hover{
            cursor: pointer;
        }
    </style>
</header>
<body>
	<form action="addemployee.php" method="POST">
		<table>
			<th colspan='2'>ADD NEW EMPLOYEE<th>
			<tr>
				<td><label>First Name: </label></td>
				<td><input type='text' name='firstname' required></td>
			</tr>
			<tr>
				<td><label>Middle Name: </label></td>
				<td><input type='text' name='midname'></td>
			</tr>
			<tr>
				<td><label>Last Name: </label></td>
				<td><input type='text' name='lastname' required></td>
			</tr>
			<tr>
				<td><label>Department: </label></td>
				<td>
					<select name="dept" required>
						<option>Live Stock Deptartment</option>
						<option>Palay Lands (Humayan)</option>
						<option>Motorpool</option>
						<option>Vegetables Farm</option>
						<option>Coconut Area</option>
						<option>Sugarcane</option>
						<option>Voucher Employees</option>
					</select>
				</td>
			</tr>
			<tr>
				<td><label>Rate: </label></td>
				<td>
					<input type="text" name="rate">
				</td>
			</tr>
			<tr align='center'>
                <td><button disabled><a href="viewemployees.php" style="text-decoration: none; color: black;">Cancel</a></button></td>
				<td><input class="btn" type='submit' name='sumbit' value='Submit'></td>
			</tr>
		</table>
	</form>
</body>
</html>