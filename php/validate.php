<?php
/**
 * Created by PhpStorm.
 * User: Johndel
 * Date: 9/20/2017
 * Time: 7:13 PM
 */

/*
 * This php file is to validate if a user has logged in and embedded
 * in this file to make it more dynamic to all pages not
 * needing to code it to every page
 *
 * */

session_start();

//check if a user is logged in
if(!(isset($_SESSION['username']))) { // if there are no username set, then go back to the index.html
    header("location: /payroll/");
}