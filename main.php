<?php

include "php/dbconnect.php";
include "php/validate.php"; // check if user is online

?>

<html>
    
    <link href="style.css" type="text/css" rel="stylesheet" />
    
    <header>
        <title>Welcome to Payroll System</title>
    </header>
    
    <body class="main_body">
        <div class="header">
            
            <div class="banner_UI"> <a href="main.php"><img src="img/banner.png" width="120" /></a> </div>
            
            <div class="header_UI">
                <table>
                    <tr>
                        <td>
                            <table class="user_info">
                                <tr>
                                    <td>
                                        <?php
                                            if(isset($_SESSION['username'])){
                                                echo "<p>".$_SESSION['fname']." ".$_SESSION['lname']."</p>";
                                            }
                                        ?>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <?php
                                                if(isset($_SESSION['username'])){

                                                    $level;

                                                    switch ($_SESSION['access_level']){

                                                        case 1:
                                                            $level = "Administrator";
                                                            break;

                                                        case 2:
                                                            $level = "Company Officer";
                                                            break;

                                                        case 3:
                                                            $level = "Employee";
                                                            break;
                                                    }
                                                echo "<p><i>".$level."</i></p>";
                                            }
                                        ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        
                        <td>
                            <form action="" method="post">
                                <input type="submit" name="logout" value="LOG OUT" class="btn_logout" />
                            </form>
                        </td>
                    </tr>
                </table>
            </div>
            
        </div>
        
        <div class="body">
            <table>
                <td>
                    <table class="sidebar">
                        <tr>
                            <td>
                                <a href="register.php">
                                    <button class="btnMenu">
                                        Register
                                    </button>
                                </a>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <a href="employees/viewemployees.php">
                                    <button class="btnMenu">
                                        Employee
                                    </button>
                                </a>

                            </td>
                        </tr>

                        <tr>
                            <td>
                                <a href="payroll/pickpayroll.php">
                                    <button class="btnMenu">
                                        Payroll
                                    </button>
                                </a>

                            </td>
                        </tr>

                        <tr>
                            <td>
                                <a href="sss/viewreports.php">
                                    <button class="btnMenu">
                                        SSS
                                    </button>
                                </a>
                            </td>
                        </tr>
                    </table>
                </td>
            </table>
        </div>
        
        <div class="footer">footer
        </div>
    </body>
</html>

<?php

// logout button press
if(isset($_POST['logout'])){ // destroy all sessions and go back to index.html
    session_destroy();
    header('location: index.html');
}

?>