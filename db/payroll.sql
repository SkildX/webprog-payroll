-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 22, 2017 at 09:35 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `payroll`
--

-- --------------------------------------------------------

--
-- Table structure for table `cutoff`
--

CREATE TABLE `cutoff` (
  `cutoffID` int(10) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cutoff`
--

INSERT INTO `cutoff` (`cutoffID`, `start_date`, `end_date`) VALUES
(1, '2017-10-02', '2017-10-27'),
(2, '2017-10-03', '2017-10-17'),
(10, '2017-10-04', '2017-10-25'),
(11, '2017-10-01', '2017-10-22'),
(12, '2017-10-15', '2017-10-22'),
(13, '2017-10-02', '2017-10-03');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `emp_id` int(10) NOT NULL,
  `fname` varchar(15) NOT NULL,
  `lname` varchar(15) NOT NULL,
  `mname` varchar(10) DEFAULT NULL,
  `department` varchar(25) NOT NULL,
  `rate` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`emp_id`, `fname`, `lname`, `mname`, `department`, `rate`) VALUES
(1, 'Johndel', 'Villanueva', 'Bendol', 'Voucher Employees', 115),
(2, 'John Joy', 'Delos Lingco', 'Lance', 'Live Stock Department', 350),
(5, 'New', 'Matrix', 'Middle', 'Live Stock Department', 350),
(6, 'JJ', 'Napoleon', 'Delos', 'Voucher Employees', 115);

-- --------------------------------------------------------

--
-- Table structure for table `payroll`
--

CREATE TABLE `payroll` (
  `emp_id` int(10) NOT NULL,
  `duty_days` int(10) NOT NULL,
  `cash_adv` double NOT NULL,
  `cutoffID` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payroll`
--

INSERT INTO `payroll` (`emp_id`, `duty_days`, `cash_adv`, `cutoffID`) VALUES
(2, 12, 200, 1),
(2, 15, 250, 2);

-- --------------------------------------------------------

--
-- Table structure for table `range`
--

CREATE TABLE `range` (
  `compensation` varchar(50) NOT NULL,
  `ee` double(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `range`
--

INSERT INTO `range` (`compensation`, `ee`) VALUES
('1,000 - 1,249.99', 36.30),
('1,250 - 1,749.99', 54.50),
('1,750 - 2,249.99', 72.70),
('2,250 - 2,749.99', 90.80),
('2,750 - 3,249.99', 109.00),
('3,250 - 3,749.99', 127.20),
('3,750 - 4,249.99', 145.30),
('4,250 - 4,749.99', 163.50),
('4,750 - 5,249.99', 181.70),
('5,250 - 5,749.99', 199.80),
('5,750 - 6,249.99', 218.00),
('6,250 - 6,749.99', 236.20),
('6,750 - 7,249.99', 254.30),
('7,250 - 7,749.99', 272.50),
('7,750 - 8,249.99', 290.70),
('8,250 - 8,749.99', 308.80),
('8,750 - 9,249.99', 327.00),
('9,250 - 9,749.99', 345.20),
('9,750 - 10,249.99', 363.30),
('10,250 - 10,749.99', 381.50),
('10,750 - 11,249.99', 399.70),
('11,250 - 11,749.99', 417.80),
('11,750 - 12,249.99', 436.00),
('12,250 - 12,749.99', 454.20),
('12,750 - 13,249.99', 472.30),
('13,250 - 13,749.99', 490.50),
('13,750 - 14,249.99', 508.70),
('14,250 - 14,749.99', 526.80),
('14,750 - 15,249.99', 545.00),
('15,250 - 15,749.99', 563.20),
('15,750 - over', 581.30);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(20) NOT NULL,
  `fname` varchar(15) NOT NULL,
  `lname` varchar(15) NOT NULL,
  `mname` varchar(10) DEFAULT NULL,
  `access_level` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Table for system administrators & officers.';

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `mname`, `access_level`) VALUES
(1, 'admin', '1234', 'Boss', 'Mommy', NULL, 1),
(2, 'SkildX', 'password', 'Johndel', 'Villanueva', 'Bendol', 2),
(5, 'Cycle', 'password1', 'Johndel', 'Villanueva', 'Bendol', 3),
(6, 'jv', 'password2', 'John Joy', 'Ignacio', '', 1),
(7, 'kez', 'password2', 'Keziah Ruth', 'Lingco', 'D', 3),
(11, 'lem', 'password3', 'Lemuel', 'Aloro', 'John', 2),
(12, '123', '456', '789', '345', '012', 2),
(13, 'CycleGround123', 'jasper', 'Jasper Lance', 'Dayon', '', 1),
(14, '12345', 'password', 'Elzio', 'Creed', 'Assasino', 2),
(15, 'Something', '12345', 'To', 'On', 'Chew', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cutoff`
--
ALTER TABLE `cutoff`
  ADD PRIMARY KEY (`cutoffID`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`emp_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cutoff`
--
ALTER TABLE `cutoff`
  MODIFY `cutoffID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `emp_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
