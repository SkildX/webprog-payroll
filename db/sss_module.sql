-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 27, 2017 at 08:37 AM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sss_module`
--

-- --------------------------------------------------------

--
-- Table structure for table `range`
--

CREATE TABLE IF NOT EXISTS `range` (
  `compensation` varchar(50) NOT NULL,
  `ee` double(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `range`
--

INSERT INTO `range` (`compensation`, `ee`) VALUES
('1,000 - 1,249.99', 36.30),
('1,250 - 1,749.99', 54.50),
('1,750 - 2,249.99', 72.70),
('2,250 - 2,749.99', 90.80),
('2,750 - 3,249.99', 109.00),
('3,250 - 3,749.99', 127.20),
('3,750 - 4,249.99', 145.30),
('4,250 - 4,749.99', 163.50),
('4,750 - 5,249.99', 181.70),
('5,250 - 5,749.99', 199.80),
('5,750 - 6,249.99', 218.00),
('6,250 - 6,749.99', 236.20),
('6,750 - 7,249.99', 254.30),
('7,250 - 7,749.99', 272.50),
('7,750 - 8,249.99', 290.70),
('8,250 - 8,749.99', 308.80),
('8,750 - 9,249.99', 327.00),
('9,250 - 9,749.99', 345.20),
('9,750 - 10,249.99', 363.30),
('10,250 - 10,749.99', 381.50),
('10,750 - 11,249.99', 399.70),
('11,250 - 11,749.99', 417.80),
('11,750 - 12,249.99', 436.00),
('12,250 - 12,749.99', 454.20),
('12,750 - 13,249.99', 472.30),
('13,250 - 13,749.99', 490.50),
('13,750 - 14,249.99', 508.70),
('14,250 - 14,749.99', 526.80),
('14,750 - 15,249.99', 545.00),
('15,250 - 15,749.99', 563.20),
('15,750 - over', 581.30);
